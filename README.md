# Sommaire

- [Sommaire](#sommaire)
- [Règles stuff](#règles-stuff)
  - [Classes DofusBook](#classes-dofusbook)
  - [Tags DofusBook](#tags-dofusbook)
  - [Dossier DofusBook](#dossier-dofusbook)
  - [Exos](#exos)
  - [Slot trophées/dofus](#slot-trophéesdofus)
  - [Boucliers](#boucliers)
  - [Stuffs Retrait Esquive Tacle Fuite](#stuffs-retrait-esquive-tacle-fuite)
- [Nomenclature](#nomenclature)
  - [Exemples de nom](#exemples-de-nom)

# Règles stuff

Les règles pour que la comparaison de stuff soit pertinente :

## Classes DofusBook

- on ne précise pas la classe sauf si le stuff est très spécifique
  - notamment pour les passages

## Tags DofusBook

- tagger les stuffs au plus précis possible

## Dossier DofusBook

**Dossier** : 200, 199, Sagesse, Passage, Prospection

Les recherches de stuffs seront effectuées dans le dossier 200 par défaut.

## Exos

- PA/PM autorisé
- pas de % do
- exos spécifiques autorisés
  - %crit pour monter à un taux %crit satisfaisant
  - %res mêlée/distance/élément ou réCri ou réPou ou réFix pour stuff tank/HT particulier
  - pour faire des stuffs extrême
    - stuff 4000 Do Eau par ex

## Slot trophées/dofus

- uniquement ce qui représente le stuff
- pas de : Abyssal, Nébu, Cauchemar, ...
- préférer les slots libres plutôt que l'hésitation

## Boucliers

- mettre le vrai shield si nécessaire dans le stuff
  - par exemple bouclier res pour équilibrer res sur stuff Tank ou HT
  - bouclier nécessaire pour panoplie
- quand c'est pas le cas : Stalak en no crit, 4F en crit

## Stuffs Retrait Esquive Tacle Fuite

- pas de trophées retrait/esquive/tacle/fuite (slot libre)
- pas de fami retrait/esquive/tacle/fuite (slot libre)
  - sauf si c'est vraiment le fami joué 99% du temps

# Nomenclature

**Nom stuff** : `Tankiness [Elément] [Spécificité] PA/PM/PO (NL|L) [PML] [Dist/CaC] [Légende] [LowCost] [OptiMax]`

`Tankiness` :

- `Tank` : stuff uniquement dédié au tanking avec 0 dmg
- `HT` : High Tanky : 4500+ HP 35/45% res all
- `MT` : Mid Tanky : 4000+ HP 25/35% res all
- `Chips` : uniquement orienté dmg

`Elément` :

- pour tank no élément : `Tank`
- pour mono : `Air`
- pour bi/tri élém : `Air/Terre/Feu`
- pour multi : `Multi`

`Spécificité` :

- possibilités (si plusieurs, elles doivent apparaître dans cet ordre):

- `RetPA` `RetPM`
- `EsqPA` `EsqPM`
- `Fui`
- `Tac`
- `RéFi`
- `RéPou`
- `RéCri`
- `DoCri` (on écrit pas NoCri, c'est implicite)
- `RéMe`
- `RéDi`
- `Soins`
- `CaCHeal` si vraiment le stuff est centré autour
- `100` : 95% crit sur le stuff
- `donjon` : le donjon pour lequel le stuff passage est utilisé
- `classe` : la classe spécifique qui joue le stuff (passage essentiellement)
  - on écrit le nom de la classe raccourci (Iop, Féca, Eni, Enu, Hupper, Elio, Panda, Sadi, Roub, Zobal, Forge, Sram, Eca, Cra, Steam, Ougi, Xel, Osa)

`PA/PM/PO` :

- `PA` : quantité indiquée (0-12)
- `PM` : quantité indiquée (0-6)
- `PO` : CAC (0 1 2) OU Mid (3 4 5) OU 6

> Le `+` est pas autorisé dans le nom déso.

`NL|L` :

- `NL` : le stuff n'a pas d'item qui lock à 11PA
- `L`: le stuff a un item qui lock à 11PA

`PML` : uniquement si le stuff est lock à 5PM (ou moins)

`Dist/CaC` : uniquement si le stuff est vraiment très orienté sur l'un ou l'autre

`Légende` : indique les légendes du stuff

- `Croco`
- `Jhess`
- `Jah`
- `Corru`
- `Gany`
- `Guerre`
- `Fall`
- `Mille`
- `Cul`
- `Fléau`
- `Dodge`
- `Brame`
- `Buho`
- `Servi`
- `Trompe`
- `Rykke`

`LowCost` : sans exo ni Ocre

`OptiMax` : FM tryhard et spécifique, full exo, le truc de zinzin

## Exemples de nom

- Le classique alli/vv : `Chips Air 12/6/Mid NL`
- Un stuff panda tank : `Tank 11/6/CaC L Jhess`
- Un stuff full dmg air : `Chips Air DoAir 11/6/CaC L`
- Un stuff élio feu classique : `MT Feu 11/6/CaC L`
- Le stuff multi : `Chips Multi DoCrit 100 11/6/Mid NL`
- Un stuff retrait PM : `MT Air/Terre RetPM 12/6/Mid NL`
